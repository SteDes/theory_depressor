reset

load '/media/sf_Austausch/latexfiles/gnuplotlayout.gp'
set output 'Mptoa.tex'

set grid

#filef(x)=sprintf("< sed 's/(//g; s/)//g' /media/sdeschner/Data/OpenFoamSimulationen/parstud_NACA16-455_all/depressor_v_%d.0/comb_dataf.dat",x)
filef(x)=sprintf("< sed 's/(//g; s/)//g' data/comb_dataf_v%d.dat",x)

## https://www.cfd-online.com/Forums/openfoam-solving/84125-negative-drag-coefficient.html
cl_p(cl,cd,a)=cl*cos(a*pi/180.)+cd*sin(a*pi/180.)
cd_p(cl,cd,a)=cl*sin(a*pi/180.)-cd*cos(a*pi/180.)


set key b r samplen 1 #maxrows 4
set xrange[-22:22]
#set yrange[-5000:5000]
set xlabel '$AoA / \alpha$'
#set logscale y
set ylabel '$M_{Pz} / \si{\newton\times\meter}$'


# gw steht für Grenzwert, da manche Datensätze doch nicht konvergierte Lösungen enthalten
gw=5000#1E+4
# Satz von Steiner
SVS=0
M=13

p for [v=1:5] filef(v) u 20:(abs(column(M))<gw?column(M):1/0) w lp lc rgb col[v] pt 6 lw 3 t sprintf('$v_0=\SI{%d}{\meter\per\second}$',v),\

#p for [v=1:5] filef(v) u 20:(column(M)<0?abs(column(M)<gw?column(M):1/0-SVS):1/0) w lp lc rgb col[v] pt 6 lw 3 t sprintf('$v_0=\SI{%d}{\meter\per\second}$',v),\
  for [v=1:5] filef(v) u 20:(column(M)>=0?abs(column(M)<gw?column(M):1/0-SVS):1/0) w lp lc rgb col[v] pt 7 lw 3 not,\
  filef(1) u 20:(column(M)<0?abs(column(M)<gw?column(M):1/0-SVS):1/0) w lp lc rgb col[1] pt 6 lw 3 t sprintf('$neg. Moments$',v),\
  filef(1) u 20:(column(M)>=0?abs(column(M)<gw?column(M):1/0-SVS):1/0) w lp lc rgb col[1] pt 7 lw 3 t sprintf('$pos. Moments$',v)#,\
         filef(1) u 20:(column(M)>=0?abs(column(M)<gw?column(M):1/0-SVS):1/0):(sprintf("%d",$20)) every 1 w labels center offset 0,-2 tc rgb col[1] not,\
         filef(1) u 20:(column(M)<0?abs(column(M)<gw?column(M):1/0-SVS):1/0):(sprintf("%d",$20)) every 1 w labels center offset 0,-2 tc rgb colT[1] not#,\
         filef(5) u 20:(column(M)<0?abs(column(M)<gw?column(M):1/0-SVS):1/0):(sprintf("%d",$20)) every 1 w labels center offset 0,2 tc rgb colT[5] not,\
         filef(5) u 20:(column(M)>=0?abs(column(M)<gw?column(M):1/0-SVS):1/0):(sprintf("%d",$20)) every 1 w labels center offset 0,2 tc rgb col[5] not#,\

#EOF
