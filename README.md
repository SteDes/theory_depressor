#Theory Depressor

This latex-document describes the theory behind the SST-depressor.
It includes

- the gnuplot scripts
- the latex sourcecode
- a `makefile`

to generate the figs and the pdf files via

- `make figs` to run gnuplot on the scripts
- `make bib` to run biblatex for the references
- `make pdf` to generate the pdf-document
- `make all` to do all described steps at once

**In general, there is no need to newly generate the pdf because its current status is always tracked.**

---

#Caution
~~**The datafiles are not added yet!**~~

~~Plese just run `make pdf` until these files are added. Otherwise the plots cannot be generated.~~

Should be added in the meantime. Not approved yet!

