reset

load '/media/sf_Austausch/latexfiles/gnuplotlayout.gp'
set output 'Fpytoa.tex'

set grid

#filef(x)=sprintf("< sed 's/(//g; s/)//g' /media/sdeschner/Data/OpenFoamSimulationen/parstud_NACA16-455_all/depressor_v_%d.0/comb_dataf.dat",x)
filef(x)=sprintf("< sed 's/(//g; s/)//g' data/comb_dataf_v%d.dat",x)

## https://www.cfd-online.com/Forums/openfoam-solving/84125-negative-drag-coefficient.html
cl_p(cl,cd,a)=cl*cos(a*pi/180.)+cd*sin(a*pi/180.)
cd_p(cl,cd,a)=cl*sin(a*pi/180.)-cd*cos(a*pi/180.)


set key b r samplen 1 #maxrows 4
set xrange[-22:22]
set yrange[-.1:1E4]
set xlabel '$AoA / \alpha$'
set logscale y
set ylabel '$\left|F_{y}\right| / \si{\newton}$'


# gw steht für Grenzwert, da manche Datensätze doch nicht konvergierte Lösungen enthalten
gw=1E+4
# Satz von Steiner
SVS=0

p for [v=1:5] filef(v) u 20:(abs($3)<gw?$3+$6:1/0) w lp lc rgb col[v] pt 7 lw 3 t sprintf('$v_0=\SI{%d}{\meter\per\second}$',v),\
  for [v=1:5] filef(v) u 20:(abs($3)<gw?abs($3+$6):1/0) w lp lc rgb col[v] pt 6 lw 3 not sprintf('$v_0=\SI{%d}{\meter\per\second}$',v),\
  '' u (-25):(1E-2) w lp lc rgb col[1] pt 6 lw 3 t '$F_{y}<0$',\
  '' u (-25):(1E-2) w lp lc rgb col[1] pt 7 lw 3 t '$F_{y}>0$'


#EOF
