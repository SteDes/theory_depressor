reset

load '/media/sf_Austausch/latexfiles/gnuplotlayout.gp'
set output 'cltocd.tex'

## https://www.cfd-online.com/Forums/openfoam-solving/84125-negative-drag-coefficient.html
cl_p(cl,cd,a)=cl*cos(a*pi/180.)+cd*sin(a*pi/180.)
cd_p(cl,cd,a)=cl*sin(a*pi/180.)-cd*cos(a*pi/180.)

set grid

#file(x)=sprintf("/media/sdeschner/Data/OpenFoamSimulationen/parstud_NACA16-455_all/depressor_v_%d.0/comb_dataC.dat",x)
file(x)=sprintf("data/comb_dataC_v%d.dat",x)

#stats filemean(foldername(alpha)) u 3 name "cw" nooutput
#stats filemean(foldername(alpha)) u 4 name "cl" nooutput



set key c r samplen 1 #maxrows 4
set xrange[0:0.3]
set yrange[-1:1]
set xlabel '$c_D$'
set ylabel '$c_L$'

# gw steht für Grenzwert, da manche Datensätze doch nicht konvergierte Lösungen enthalten
gw=1E+1

set arrow 1 from 0,0 to 0.3,.83 lc rgb col[1] nohead
set arrow 2 from 0,0 to 0.265,-1  lc rgb col[1] nohead
set arrow 3 from 0,0 to 0.24,1. lc rgb col[5] nohead
set arrow 4 from 0,0 to 0.145,-1  lc rgb col[5] nohead

p for [v=1:5] file(v) u (cd_p($4<gw?$4:1/0,$3<gw?$3:1/0,$7)):(cl_p($4<gw?$4:1/0,$3<gw?$3:1/0,$7)) w lp lc rgb col[v] pt 7 lw 3 t sprintf('$v_0=\SI{%d}{\meter\per\second}$',v),\
	file(5) u (cd_p($4<gw?$4:1/0,$3<gw?$3:1/0,$7)):(cl_p($4<gw?$4:1/0,$3<gw?$3:1/0,$7)):(sprintf("%d",$7)) every 5::0::20 w labels center offset 0,-2 tc rgb col[5] t sprintf('numbers: AoA / $\alpha$'),\
	file(5) u (cd_p($4<gw?$4:1/0,$3<gw?$3:1/0,$7)):(cl_p($4<gw?$4:1/0,$3<gw?$3:1/0,$7)):(sprintf("%d",$7)) every 5::20::40 w labels center offset 0,2 tc rgb col[5] not sprintf('numbers: AoA $\alpha$'),\


#	file(1) u (cd_p($4<gw?$4:1/0,$3<gw?$3:1/0,$7)):(cl_p($4<gw?$4:1/0,$3<gw?$3:1/0,$7)):(sprintf("%d",$7)) every 5 w labels center offset 5,0 tc rgb col[1] not

#p for [v=1:5] file(v) u ($3<gw?$3:1/0):($4<gw?$4:1/0) w lp lc rgb col[v] pt 7 lw 3 t sprintf('$v_0=\SI{%d}{\meter\per\second}$',v),\
	file(5) u 3:4:(sprintf("%d",$7)) every 3 w labels center offset 3,0 tc rgb col[5] t sprintf('numbers: AoA $\alpha$'),\
	file(1) u 3:4:(sprintf("%d",$7)) every 3 w labels center offset -4,0 tc rgb col[1] not


#EOF
