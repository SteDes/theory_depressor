reset

load '/media/sf_Austausch/latexfiles/gnuplotlayout.gp'
set output 'clcdtoa.tex'

set grid

#file(x)=sprintf("/media/sdeschner/Data/OpenFoamSimulationen/parstud_NACA16-455_all/depressor_v_%d.0/comb_dataC.dat",x)
file(x)=sprintf("data/comb_dataC_v%d.dat",x)

## https://www.cfd-online.com/Forums/openfoam-solving/84125-negative-drag-coefficient.html
cl_p(cl,cd,a)=cl*cos(a*pi/180.)+cd*sin(a*pi/180.)
cd_p(cl,cd,a)=cl*sin(a*pi/180.)-cd*cos(a*pi/180.)

set key c r samplen 1 #maxrows 4
#set xrange[-.1:.1]
#set yrange[-1:1]
set xlabel '$AoA / \alpha$'
set ylabel '$c_L/c_D$'
#set logscale y

# gw steht für Grenzwert, da manche Datensätze doch nicht konvergierte Lösungen enthalten
gw=1E+1

p for [v=1:5] file(v) u ($7):(cl_p($4,$3,$7)/cd_p($4,$3,$7)<gw?cl_p($4,$3,$7)/cd_p($4,$3,$7):1/0) w lp lc rgb col[v] pt 7 lw 3 t sprintf('$v_0=\SI{%d}{\meter\per\second}$',v)

#p for [v=1:5] file(v) u ($7):($4/$3>0?abs($4/$3<gw?$4/$3:1/0):1/0) w lp lc rgb col[v] pt 7 lw 3 t sprintf('$v_0=\SI{%d}{\meter\per\second}$',v),\
  for [v=1:5] file(v) u ($7):($4/$3<=0?abs($4/$3<gw?$4/$3:1/0):1/0) w lp lc rgb colT[v] pt 7 lw 3 t sprintf('$v_0=\SI{%d}{\meter\per\second}$',v)

#EOF
