reset

load '/media/sf_Austausch/latexfiles/gnuplotlayout.gp'
set output 'Fpxztoa.tex'

set grid

#filef(x)=sprintf("< sed 's/(//g; s/)//g' /media/sdeschner/Data/OpenFoamSimulationen/parstud_NACA16-455_all/depressor_v_%d.0/comb_dataf.dat",x)
filef(x)=sprintf("< sed 's/(//g; s/)//g' data/comb_dataf_v%d.dat",x)

## https://www.cfd-online.com/Forums/openfoam-solving/84125-negative-drag-coefficient.html
cl_p(cl,cd,a)=cl*cos(a*pi/180.)+cd*sin(a*pi/180.)
cd_p(cl,cd,a)=cl*sin(a*pi/180.)-cd*cos(a*pi/180.)

set ytics nomirror
set y2tics nomirror

set key b l samplen 1 maxrows 3
set xrange[-22:22]
set yrange[-1400:1000]
set y2range[-4:10]
set xlabel '$AoA / \alpha$'
#set logscale y
set ylabel '$F_{x} / \si{\newton}$'
set y2label '$F_{z} / \si{\newton}$'


# Satz von Steiner
SVS=0

p for [v=1:5] filef(v) u 20:(abs($2)<1E3?-$2-$5:1/0) w lp lc rgb col[v] pt 6 lw 3 axis x1y1 t sprintf('$v_0=\SI{%d}{\meter\per\second}$',v),\
  for [v=1:5] filef(v) u 20:(abs($4)<1E0?$4-$7:1/0) w lp lc rgb col[v] pt 7 lw 3 axis x1y2 not sprintf('$v_0=\SI{%d}{\meter\per\second}$',v),\
  '' u (1E5):(1E5) w lp lc rgb col[1] pt 6 lw 3 t '$F_{x}$',\
  '' u (1E5):(1E5) w lp lc rgb col[1] pt 7 lw 3 t '$F_{z}$',\




#EOF
