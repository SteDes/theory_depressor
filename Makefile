## setze gnuplot grafiken

FIG := $(wildcard *.gp)
MAIN := paramstud.tex
BIBMAIN := $(patsubst %.tex,%,$(MAIN))

ALLFIGS:= $(patsubst %.gp,%,$(OUTFIG))

print:
	@echo $(FIG)

GP := gnuplot -e " set terminal epslatex color colortext"

figs:
	$(GP) $(FIG)
	
	@echo ""
	@echo "THE FIGURES $(FIG) GOT GENERATED!"
	@echo ""

BIB := biber $(BIBMAIN)

bib:
	$(BIB)
	@echo ""
	@echo "THE BIB FILES $(MAIN) GOT GENERATED!"
	@echo ""

LATEX := pdflatex --shell-escape $(MAIN)

pdf:
	$(LATEX)
	@echo ""
	@echo "THE LATEX FILE $(MAIN) GOT GENERATED!"
	@echo ""

all:
	$(GP) $(FIG)
	$(LATEX)
	$(BIB)
	$(LATEX)
	$(LATEX)

## clean ALL temp files 
clean:
	rm -rf	 		*.blg \
   					*.aux \
					*.bcf \
					*.log \
					*.aux \
					*.xml \
					*.bbl \
					*.out \
					*.toc \
					*.eps \
					*eps-converted-to.pdf


.PHONY: figs print pdf clean

#EOF
