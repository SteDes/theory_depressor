reset

load '/media/sf_Austausch/latexfiles/gnuplotlayout.gp'
set output 'clandcdtoa.tex'

## https://www.cfd-online.com/Forums/openfoam-solving/84125-negative-drag-coefficient.html
cl_p(cl,cd,a)=cl*cos(a*pi/180.)+cd*sin(a*pi/180.)
cd_p(cl,cd,a)=cl*sin(a*pi/180.)-cd*cos(a*pi/180.)

set grid

#file(x)=sprintf("/media/sdeschner/Data/OpenFoamSimulationen/parstud_NACA16-455_all/depressor_v_%d.0/comb_dataC.dat",x)
file(x)=sprintf("data/comb_dataC_v%d.dat",x)

#stats filemean(foldername(alpha)) u 3 name "cw" nooutput
#stats filemean(foldername(alpha)) u 4 name "cl" nooutput


set ytics nomirror
set y2tics nomirror

set key t c samplen 1 #maxrows 4
set xrange[-21:21]
set yrange[-1:1]
set y2range[0:.3]
#set logscale y
set xlabel '$AoA / \alpha$'
set ylabel '$c_L$'
set y2label '$c_D$'


# gw steht für Grenzwert, da manche Datensätze doch nicht konvergierte Lösungen enthalten
gw=1E+1


p for [v=1:5] file(v) u ($7):((cl_p($4<gw?$4:1/0,$3<gw?$3:1/0,$7))) w lp lc rgb col[v] pt 7 lw 3 t sprintf('$v_0=\SI{%d}{\meter\per\second}$',v) axis x1y1,\
  for [v=1:5] file(v) u ($7):((cd_p($4<gw?$4:1/0,$3<gw?$3:1/0,$7))) w lp lc rgb col[v] pt 6 lw 3 not sprintf('$v_0=\SI{%d}{\meter\per\second}$',v) axis x1y2,\
	file(1) u (1E5):(1E5) w lp lc rgb col[1] pt 7 lw 3 t '$c_L$',\
	file(1) u (1E5):(1E5) w lp lc rgb col[1] pt 6 lw 3t '$c_D$',\


#	file(1) u (cd_p($4<gw?$4:1/0,$3<gw?$3:1/0,$7)):(cl_p($4<gw?$4:1/0,$3<gw?$3:1/0,$7)):(sprintf("%d",$7)) every 5 w labels center offset 5,0 tc rgb col[1] not

#p for [v=1:5] file(v) u ($3<gw?$3:1/0):($4<gw?$4:1/0) w lp lc rgb col[v] pt 7 lw 3 t sprintf('$v_0=\SI{%d}{\meter\per\second}$',v),\
	file(5) u 3:4:(sprintf("%d",$7)) every 3 w labels center offset 3,0 tc rgb col[5] t sprintf('numbers: AoA $\alpha$'),\
	file(1) u 3:4:(sprintf("%d",$7)) every 3 w labels center offset -4,0 tc rgb col[1] not


#EOF
